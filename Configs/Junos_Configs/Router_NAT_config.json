## Config for NAT with a webserver
 version 12.1X47-D15.4;
 system {
    host-name vSRX-XX; /* Change the name to fit the physical SRX */
    root-authentication {
        encrypted-password "$1$q9fEp/ix$/sctvx3BdBIhBVp9ySlVI1"; ## Password: Rootpass
    }
 services {
        ssh;
        dhcp-local-server {
            group DMZ {
                interface ge-0/0/3.0;
            }
            group USERLAN {
                interface ge-0/0/2.0;
            }
			group USERS {
                interface ge-0/0/1.0;
            }
        }
    }
}
interfaces {
	ge-0/0/0 {
        unit 0 {
            family inet {
				/* Untrust zone */
                address 10.56.16.99/22;
            }
        }
    }
    ge-0/0/1 {
        unit 0 {
            family inet {
				/* DMZ zone */
                address 10.10.10.2/24;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
				/* Trust zone USERLAN */
                address 10.10.12.1/24;
            }
        }
    }
	ge-0/0/3 {
        unit 0 {
            family inet {
				/* DMZ zone */
                address 10.10.11.1/24;
            }
        }
    }
}
routing-options {
    static {
        /* Route to school router gateway */
        route 0.0.0.0/0 next-hop 10.56.16.1;
		route 0.0.0.0/0 next-hop 10.10.10.1;

		
    }
}
security {
    nat {
        /* Changes the source address of egress packets */
        source {
            /* Multiple sets of rules can be set */
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                /* Multiple rules can be set in each rule-set */
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            /* Use egress interface source address */
							/* ge-0/0/0 is egress interface in untrust zone */
                            interface;
                        }
                    }
                }
            }
        /* Changes the source address of egress packets from DMZ */
		/* This is not used as the DMZ devices should not have internet access */
            rule-set DMZ-to-untrust {
                from zone DMZ;
                to zone untrust;
                /* Multiple rules can be set in each rule-set */
                rule rule-is-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            /* Use egress interface source address */
							/* ge-0/0/0 is egress interface in untrust zone */
                            interface;
                        }
                    }
                }
            }
        }
		/* Changes the destination address of ingress packets to DMZ */
        destination {
            pool NginxWebServer {
                address 10.10.11.5/32;
            }
            rule-set DestinationNATRuleWebServer {
                from zone untrust;
                rule ruleToWebServer {
                    match {
                        destination-address 10.56.16.99/32;
                        destination-port 80;
                    }
                    then {
                        destination-nat {
                            pool {
                                NginxWebServer;
                            }
                        }
                    }
                }
            }
        }
	}
    policies {
        from-zone untrust to-zone DMZ {
            policy WebServerPolicy {
                match {
                    source-address any;
                    destination-address WebServer;
					/* Only allow http trafic for the webserver. */
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone trust {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
		from-zone DMZ to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
		from-zone DMZ to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
		from-zone trust to-zone DMZ {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone DMZ {
		    address-book {
                address WebServer 10.10.11.11/32;
            }
            interfaces {
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                            dhcp;
                        }
                    }
                }
            }
        }
        security-zone trust {
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                            dhcp;
                        }
                    }
                }
				ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                            dhcp;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/0.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            ssh;
                        }
                    }
                }
            }
        }
    }
}
access {
	/* DHCP parameters to be handed out. */
    address-assignment {
        pool DMZ {
            family inet {
                network 10.10.11.0/24;
                range USERS {
                    low 10.10.11.10;
                    high 10.10.11.20;
                }
                dhcp-attributes {
                    maximum-lease-time 60;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        10.10.11.1;
                    }
                }
            }
        }
        pool USERLAN {
            family inet {
                network 10.10.12.0/24;
                range USERS {
                    low 10.10.12.10;
                    high 10.10.12.20;
                }
                dhcp-attributes {
                    maximum-lease-time 3600;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        10.10.12.1;
                    }
                }
            }
        }
		pool USERS {
            family inet {
                network 10.10.13.0/24;
                range USERS {
                    low 10.10.13.10;
                    high 10.10.13.20;
                }
                dhcp-attributes {
                    maximum-lease-time 3600;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        10.10.13.1;
                    }
                }
            }
        }
    }
}
